# cielo

API de integração com o webservice da cielo.
Versão da API da cielo utilizada é a 1.2.1
Datas no formato: YYYY-MM-DDTHH-mm-ss.SSSZ


### createCardToken
- Viabiliza o envio de transações sem o cartão.
- Para os mesmos dados de cartão, o token enviado é o mesmo.
- A transação feita via token NÃO isenta o lojista do envio da informação de bandeira,
portanto é necessário que o sistema do lojista (ou gateway) que armazenará os tokens também
armazene a bandeira do cartão que foi tokenizado.
- Um token não utilizado por mais de um ano será automaticamente removido do banco de dados da Cielo.
É possível realizar o bloqueio de um token específico para que este não seja mais usado. O bloqueio
do token deve ser solicitado ao Suporte Web Cielo E-commerce.
- O envio do nome do portador do cartão é opcional.

Exemplo de chamada:

```javascript
  cielo.createCardToken({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    creditCard: {
      pan: '3566007770004971',
      expirationDate: '201805',
      cardHolderName: 'Joao'
    }
  }, function _createCardToken(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta:

```javascript
{
  cielo_card_token: 'wtUE52BOoSM4+pVqzMxFugJGt2ZjDWIQEM15tc8YAlI=',
  cielo_card_token_status: '1',
  cielo_card_token_detailed_status: 'Cartão desbloqueado',
  cielo_card_masked_pan: '356600******4971'
}
```


### createTransaction
- Uma transação autorizada somente gera o crédito para o estabelecimento comercial caso ela seja
capturada. Por isso, toda venda que o lojista queira efetivar será preciso realizar a captura (ou
confirmação) da transação.
- É assumido que a transação terá UMA parcela somente.
- A transação de débito só aceita cartões visa e mastercard.
- Para transações de crédito, o campo "capture" indica se esta transação serã capturada
imediatamente ou não.
- Na modalidade de débito não existe a opção de capturar posteriormente, a transação é sempre capturada
imediatamente após a autorização.
- Caso a transação seja capturada imediatamente, o valor capturado é igual ao valor autorizado.

Exemplo de chamada para pagamento crédito com captura automática:

```javascript
  cielo.createTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    amount: '5500',
    metaId: '954756320',
    softDescriptor: 'produto X',
    createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:SS'),
    capture: true,
    type: 'credit',
    creditCard: {
      pan: '3566007770004971',
      expirationDate: '201805',
      cvv: '123',
      brand: 'jcb'
    }
  }, function _createTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```
Resposta:

```javascript
{
  cielo_tid: '10069930690006688B8A',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:26:42.039-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719672',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T15:26:42.058-03:00',
  cielo_capture_amount: '5500'
}
```


Exemplo de chamada para pagamento crédito sem captura automática:

```javascript
  cielo.createTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    amount: '5500',
    metaId: '954756320',
    softDescriptor: 'produto X',
    createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:SS'),
    capture: false,
    type: 'credit',
    creditCard: {
      pan: '3566007770004971',
      expirationDate: '201805',
      cvv: '123',
      brand: 'jcb'
    }
  }, function _createTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta:

```javascript
{
  cielo_tid: '10069930690006688C9A',
  cielo_status: '4',
  cielo_detailed_status: 'Transação autorizada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:29:34.252-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719689'
}
```


Exemplo de chamada para pagamento débito:

```javascript
  cielo.createTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    amount: '5500',
    metaId: '954756320',
    softDescriptor: 'produto X',
    createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:SS'),
    type: 'debit',
    returnURL: 'http://devapi.4all.mobi:3050/pedido',
    creditCard: {
      pan: '4012001037141112',
      expirationDate: '201805',
      cvv: '123',
      brand: 'visa'
    }
  }, function _createTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta:

```javascript
{
  cielo_tid: '10069930690006688D5A',
  cielo_status: '0',
  cielo_detailed_status: 'Transação criada',
  cielo_auth_url: 'https://qasecommerce.cielo.com.br/web/index.cbmp?id=e4b3da81e286b18e54ae1a7ab1e24c71'
}
```


### captureTransaction
- A captura pode ocorrer em dois momentos: Imediatamente após a autorização (captura total) e
após a autorização (captura total ou parcial). A captura após a autorização é realizada através desta chamada.
- O prazo máximo para realizar a captura posterior é de 5 dias corridos após a data da
autorização. Por exemplo, se uma autorização ocorreu em 10/12 às 15h20m45s, o limite para captura
será às 15h20m45s do dia 15/12.
- Só é possível utilizar o TID da transação para realizar a captura.
- Somente transações de crédito podem ser capturadas posteriormente, transações de débito são sempre
capturadas imediatamente após a autorização.
- O campo "amount" indica o valor da transação a ser capturado. Se o campo for omitido, o valor de
captura será igual ao valor autorizado.
- Não é permitido capturar novamente uma transação que já foi parcialmente ou totalmente capturada.

Exemplo de captura total:

```javascript
  cielo.captureTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    tid: '100699306900066892DA'
  }, function _captureTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta:

```javascript
{
  cielo_tid: '1006993069000668A86A',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T16:32:04.819-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '720134',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T16:32:20.238-03:00',
  cielo_capture_amount: '5500'
}
```

Exemplo de captura parcial:

```javascript
  cielo.captureTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    tid: '1006993069000668A8FA',
    amount: '1000'
  }, function _captureTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta:

```javascript
{
  cielo_tid: '1006993069000668A8FA',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T16:33:31.997-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '720143',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T16:34:06.755-03:00',
  cielo_capture_amount: '1000'
}
```


### checkTransaction
- Há duas opções de consulta que podem ser realizadas: Através do TID ou através do número do pedido.
- Somente transações dos últimos 365 dias estão disponíveis para consulta.
- A consulta por número do pedido deve ser usada apenas como contingência à consulta
por TID, pois esta pode não garantir unicidade da transação, tendo em vista que este campo é enviado
pela loja virtual e apenas acatado pela Cielo.
- Caso seja encontrada mais de uma transação para o mesmo número do pedido, a Cielo
enviará a transação mais recente.

Exemplo de consulta pelo TID:

```javascript
  cielo.checkTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    tid: '100699306900064F8E0A'
  }, function _checkTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Exemplo de consulta pelo número do pedido (metaId):

```javascript
  cielo.checkTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    metaId: '954756320'
  }, function _checkTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta (transação não autorizada):

```javascript
{
  cielo_tid: '100699306900066892DA',
  cielo_status: '0',
  cielo_detailed_status: 'Transação criada'
}
```

Resposta (transação autorizada):

```javascript
{
  cielo_tid: '1006993069000668926A',
  cielo_status: '4',
  cielo_detailed_status: 'Transação autorizada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:39:39.911-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719782'
}
```

Resposta (transação capturada totalmente):

```javascript
{
  cielo_tid: '100699306900066891CA',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:37:26.638-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719772',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T15:37:26.660-03:00',
  cielo_capture_amount: '5500'
}
```

Resposta (transação capturada parcialmente):

```javascript
{
  cielo_tid: '1006993069000668A8FA',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T16:33:31.997-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '720143',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T16:34:06.755-03:00',
  cielo_capture_amount: '1000'
}
```

Resposta (transação com múltiplos cancelamentos):

```javascript
{
  cielo_tid: '100699306900064F8E0A',
  cielo_status: '9',
  cielo_detailed_status: 'Transação cancelada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-09T17:06:26.031-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '617312',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-09T17:06:26.050-03:00',
  cielo_capture_amount: '5500',
  cielo_cancellations: [ {
    cancellation_message: 'Transacao cancelada com sucesso',
    cancellation_date: '2016-05-09T17:15:29.000-03:00',
    cancellation_amount: '500'
  }, {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-09T17:14:30.000-03:00',
    cancellation_amount: '1000'
  }, {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-09T17:14:27.000-03:00',
    cancellation_amount: '1000'
  }, {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-09T17:07:47.000-03:00',
    cancellation_amount: '1000'
  }, {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-09T17:07:25.000-03:00',
    cancellation_amount: '1000'
  }, {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-09T17:07:22.000-03:00',
    cancellation_amount: '1000'
  } ]
}
```

       
### cancelTransaction
- Se a transação estiver apenas autorizada e a loja queira cancelá-la, o pedido de
cancelamento não é necessário, pois após o prazo de captura expirar, ela será cancelada
automaticamente pelo sistema.
-  O prazo de cancelamento é de até 120 dias para a modalidade crédito e D+0 para
débito.
- Só é possível utilizar o TID da transação para realizar o cancelamento.
- O cancelamento total, quando realizado com sucesso, altera o status da transação para "9 - Cancelada",
enquanto que o parcial não altera o status da transação, mantendo-a como "6 - Capturada".
- O campo "amount" indica quanto do valor total da transação deve ser cancelado, se o campo
for omitido, é assumido que o cancelamento é total.
- Para a modalidade débito, não existe a possibilidade de efetuar cancelamento parcial.
- O cancelamento total é válido tanto para transações capturadas, como autorizadas; o parcial
é válido apenas para as capturadas.
- Na tentativa de cancelar parcialmente uma transação autorizada, o cancelamente
total é realizado.

Exemplo de cancelamento total:

```javascript
  cielo.cancelTransaction({
    merchant: {
      affiliationNumber: '1006993069',
      affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
    },
    tid: '100699306900064F8E0A'
  }, function _cancelTransaction(err, data) {
    console.log(err);
    console.log(data);
  });
```

Resposta (cancelamento total transação autorizada):

```javascript
{
  cielo_tid: '1006993069000668926A',
  cielo_status: '9',
  cielo_detailed_status: 'Transação cancelada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:39:39.911-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719782',
  cielo_cancellations: [ {
    cancellation_message: 'Transação desfeita',
    cancellation_date: '2016-05-20T15:42:41.166-03:00',
    cancellation_amount: '5500'
  } ]
}
```

Resposta (cancelamento total transação capturada):

```javascript
{
  cielo_tid: '100699306900066891CA',
  cielo_status: '9',
  cielo_detailed_status: 'Transação cancelada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:37:26.638-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719772',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T15:37:26.660-03:00',
  cielo_capture_amount: '5500',
  cielo_cancellations: [ {
    cancellation_message: 'Transacao cancelada com sucesso',
    cancellation_date: '2016-05-20T15:45:47.849-03:00',
    cancellation_amount: '5500'
  } ]
}
```

Resposta (cancelamento total transação parcialmente capturada):

```javascript
{
  cielo_tid: '1006993069000668A8FA',
  cielo_status: '9',
  cielo_detailed_status: 'Transação cancelada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T16:33:31.997-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '720143',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T16:34:06.755-03:00',
  cielo_capture_amount: '1000',
  cielo_cancellations: [ {
    cancellation_message: 'Transacao cancelada com sucesso',
    cancellation_date: '2016-05-20T16:36:54.540-03:00',
    cancellation_amount: '1000'
  } ]
}
```

Resposta (cancelamento total transação parcialmente cancelada):

```javascript
{
  cielo_tid: '100699306900066895BA',
  cielo_status: '9',
  cielo_detailed_status: 'Transação cancelada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:50:58.562-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719835',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T15:50:58.576-03:00',
  cielo_capture_amount: '5500',
  cielo_cancellations: [ {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-20T15:51:14.000-03:00',
    cancellation_amount: '1000'
  }, {
    cancellation_message: 'Transacao cancelada com sucesso',
    cancellation_date: '2016-05-20T15:52:21.315-03:00',
    cancellation_amount: '4500'
  } ]
}
```

Resposta (cancelamento parcial transação capturada):

```javascript
{
  cielo_tid: '100699306900066895BA',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:50:58.562-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719835',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T15:50:58.576-03:00',
  cielo_capture_amount: '5500',
  cielo_cancellations: [ {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-20T15:51:14.141-03:00',
    cancellation_amount: '1000'
  } ]
}
```

Resposta (cancelamento parcial transação parcialmente cancelada):

```javascript
{
  cielo_tid: '100699306900066899BA',
  cielo_status: '6',
  cielo_detailed_status: 'Transação capturada',
  cielo_authentication_message: 'Transacao sem autenticacao',
  cielo_authorization_message: 'Transação autorizada',
  cielo_authorization_date: '2016-05-20T15:58:46.039-03:00',
  cielo_authorization_amount: '5500',
  cielo_authorization_code: '00',
  cielo_authorization_detailed_code: 'Transação nacional aprovada com sucesso',
  cielo_authorization_nsu: '719899',
  cielo_capture_message: 'Transacao capturada com sucesso',
  cielo_capture_date: '2016-05-20T15:58:46.062-03:00',
  cielo_capture_amount: '5500',
  cielo_cancellations: [ {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-20T15:59:04.000-03:00',
    cancellation_amount: '1000'
  }, {
    cancellation_message: 'Cancelamento parcial realizado com sucesso',
    cancellation_date: '2016-05-20T15:59:05.516-03:00',
    cancellation_amount: '1000'
  } ]
}
```


### Refs:
https://www.cielo.com.br/wps/wcm/connect/c682298e-4518-4e2b-8945-cef23e04b5ec/Cielo-E-commerce-Manual-do-Desenvolvedor-WebService-PT-V2.5.4.pdf?MOD=AJPERES&CONVERT_TO=url&CACHEID=c682298e-4518-4e2b-8945-cef23e04b5ec

