'use strict';

var path = require('path');
var https = require('https');
var iconv = require('iconv-lite');
var js2xmlparser = require('js2xmlparser');
var xml2js = require('xml2js');

var ObjectValidator = require(path.join(__dirname, 'objectValidator'));
var config = require(path.join(__dirname, 'config.json'));

var xmlParserOptions = {
  declaration: {
    include: false
  }
};

var requestOptions = {
  hostname: config.API.hostname,
  port: 443,
  path: config.API.path,
  secureProtocol: 'TLSv1_method',
  encoding: 'utf-8',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'text/xml',
    'Accept-Charset': 'utf-8',
    'Content-Length': 0
  },
  method: 'POST'
};

function translateCieloTransactionStatus(status) {

  switch (status) {
    case '0': return 'Transação criada';
    case '1': return 'Transação em andamento';
    case '2': return 'Transação autenticada';
    case '3': return 'Transação não autenticada';
    case '4': return 'Transação autorizada';
    case '5': return 'Transação não autorizada';
    case '6': return 'Transação capturada';
    case '9': return 'Transação cancelada';
    case '10': return 'Transação em autenticação';
    case '12': return 'Transação em cancelamento';
    default: return 'Status da transação desconhecido';
  }
}

function translateCieloAuthCode(code) {

  switch (code) {
    case '00': return 'Transação nacional aprovada com sucesso';
    case '01': return 'Referida pelo banco emissor';
    case '04': return 'Existe algum tipo de restrição no cartão';
    case '05': return 'Existe algum tipo de restrição no cartão';
    case '06': return 'Falha na autorização';
    case '07': return 'Existe algum tipo de restrição no cartão';
    case '08': return 'Código de segurança incorreto';
    case '11': return 'Transação internacional aprovada com sucesso';
    case '13': return 'Valor inválido';
    case '14': return 'Digitação incorreta do número do cartão';
    case '15': return 'Banco emissor indisponível';
    case '21': return 'Cancelamento não localizado no banco emissor';
    case '41': return 'Existe algum tipo de restrição no cartão';
    case '51': return 'Saldo insuficiente';
    case '54': return 'Cartão vencido';
    case '57': return 'Existe algum tipo de restrição no cartão';
    case '60': return 'Existe algum tipo de restrição no cartão';
    case '62': return 'Existe algum tipo de restrição no cartão';
    case '78': return 'Cartão não foi desbloqueado pelo portador';
    case '82': return 'Cartão inválido';
    case '91': return 'Banco emissor indisponível';
    case '96': return 'Falha no envio da autorização';
    case 'AA': return 'Timeout na comunicação com o banco emissor';
    case 'AC': return 'Cartão de débito tentando utilizar produto crédito';
    case 'GA': return 'Referida pela Cielo';
    default: return 'Código de autorização desconhecido';
  }
}

function translateCieloErrorCode(code) {

  switch (code) {
    case '01': return 'A mensagem XML está fora do formato especificado pelo arquivo eCommerce.xsd';
    case '02': return 'Impossibilidade de autenticar uma requisição daloja virtual';
    case '03': return 'Não existe transação para o identificador informado';
    case '08': return 'O código de segurança informado na mensagem é inválido';
    case '10': return 'A transação, com ou sem cartão, está divergente com a permissão de envio dessa informação';
    case '11': return 'A transação está configurada com uma modalidade de pagamento não habilitada para a loja';
    case '12': return 'O número de parcelas solicitado ultrapassa o máximo permitido';
    case '13': return 'Flag de autorização automática incompatível com a inválida forma de pagamento solicitada';
    case '14': return 'A solicitação de Autorização Direta está inválida';
    case '15': return 'A solicitação de Autorização Direta está sem cartão';
    case '16': return 'O TID fornecido está duplicado';
    case '17': return 'O código de segurança do cartão não foi enviado (essa informação é sempre obrigatória para Amex)';
    case '18': return 'Uso incorreto do indicador de código de segurança';
    case '19': return 'A URL de Retorno é obrigatória, exceto para recorrência e autorização direta.';
    case '20': return 'Não é permitido realizar autorização para o status da transação';
    case '21': return 'Não é permitido realizar autorização, pois o prazo está vencido';
    case '22': return 'Não é permitido realizar autorização para este número de parcelas';
    case '25': return 'O resultado da Autenticação da transação não permite a solicitação de Autorização';
    case '30': return 'O status da transação não permite captura';
    case '31': return 'A captura não pode ser realizada, pois o prazo para captura está vencido';
    case '32': return 'O valor solicitado para captura não é válido';
    case '33': return 'Não foi possível realizar a captura';
    case '34': return 'O valor da taxa de embarque é obrigatório se a captura for parcial e a autorização tiver sido feita com taxa de embarque';
    case '35': return 'A bandeira utilizada na transação não tem suporte à taxa de embarque';
    case '36': return 'O produto escolhido não tem suporte à taxa de embarque';
    case '40': return 'O cancelamento não pode ser realizado, pois o prazo está vencido';
    case '42': return 'Não foi possível realizar o cancelamento';
    case '43': return 'O valor que está tentando cancelar supera o valor total capturado da transação';
    case '51': return 'As configurações da transação não permitem que a transação recorrente seja efetuada com sucesso';
    case '52': return 'O token fornecido na requisição de autorização não é válido ou está bloqueado';
    case '53': return 'O cadastro do lojista não permite o envio de transações recorrentes';
    case '54': return 'As configurações da transação não permitem que a autorização direta com uso de Token seja efetuada com sucesso';
    case '55': return 'Foi solicitado criação de Token, porém o número do cartão de crédito não foi fornecido';
    case '56': return 'Foi solicitado criação de Token, porém a validade do cartão de crédito não foi fornecida';
    case '57': return 'Falha no sistema ocorrida no momento da geração do Token';
    case '61': return 'As configurações da transação recorrente estão inválidas';
    case '77': return 'Foi solicitado uma autorização com autenticação externa, porém o campo XID não foi fornecido';
    case '78': return 'Foi solicitado uma autorização com autenticação externa, porém o campo CAVV não foi fornecido';
    case '86': return 'Foi solicitado uma autorização com autenticação externa, porém os campos CAVV e XID não foram fornecidos';
    case '87': return 'Foi solicitado uma autorização com autenticação externa, porém o campo CAVV possue um tamanho divergente';
    case '88': return 'Foi solicitado uma autorização com autenticação externa, porém o campo XID possue um tamanho divergente';
    case '89': return 'Foi solicitado uma autorização com autenticação externa, porém o campo ECI possue um tamanho divergente';
    case '90': return 'Foi solicitado uma autorização com autenticação externa, porém o campo ECI possue um valor inválido';
    case '95': return 'Falha no sistema';
    case '97': return 'Falha no sistema';
    case '98': return 'A aplicação não respondeu dentro de 25 segundos';
    case '99': return 'Falha no sistema';
    default: return 'Erro na requisição';
  }
}

exports.createCardToken = function _createCardToken(data, callback) {

  // verifica os campos da transação
  var objectValidator = new ObjectValidator({
    'merchant': {
      'value': {
        'affiliationNumber': {'types': ['string']},
        'affiliationKey': {'types': ['string']}
      }
    },
    'creditCard': {
      'value': {
        'pan': {'types': ['string']},
        'expirationDate': {'types': ['string']},
        'cardHolderName': {'isOptional': true, 'types': ['string']}
      }
    },
  });
  var err = objectValidator.validate(data);
  if (err) {
    return callback('Dados de entrada inválidos');
  }

  // cria requisição para obter token do cartão
  var req = {};

  // dados da transação inseridos na própria tag (obrigatório)
  req['@'] = {};
  req['@'].id = 'a97ab62a-7956-41ea-b03f-c2e9f612c293';
  req['@'].versao = config.API.version;

  // dados-ec (obrigatório)
  req['dados-ec'] = {};
  req['dados-ec'].numero = data.merchant.affiliationNumber; // número de afiliação da loja com a Cielo (obrigatório)
  req['dados-ec'].chave = data.merchant.affiliationKey; // chave de acesso da loja atribuída pela Cielo (obrigatório)

  // dados-portador (obrigatório)
  req['dados-portador'] = {};
  req['dados-portador'].numero = data.creditCard.pan; // número do cartão (obrigatório)
  req['dados-portador'].validade = data.creditCard.expirationDate; // validade do cartão no formato aaaamm (obrigatório)
  if (data.creditCard.hasOwnProperty('cardHolderName')) {
    req['dados-portador']['nome-portador'] = data.creditCard.cardHolderName; // nome como impresso no cartão (opcional)
  }

  // parse XML
  var xml = js2xmlparser('requisicao-token', req, xmlParserOptions);
  var xmlRequest = '<?xml version="1.0" encoding="ISO-8859-1"?>\n' + xml;
  var postData = 'mensagem=' + xmlRequest;

  // send request
  requestOptions.headers['Content-Length'] = Buffer.byteLength(postData);
  var req = https.request(requestOptions, function _request(res) {
    var data = '';
    res.on('data', function _on(chunk) {
      data += iconv.decode(chunk, 'iso-8859-1');
    });
    res.on('error', function _on(e) {
      return callback(e);
    });
    res.on('end', function _on() {

      // parse XML response
      xml2js.parseString(data, function _parseString(err, result) {
        if (err) {
          return callback('Erro na resposta da Cielo');
        }

        // if error response, check if response contains all the required fields
        var res = {};
        if (result.hasOwnProperty('erro')) {
          if (!(result.erro.hasOwnProperty('codigo') && result.erro.codigo.length > 0
                && result.erro.hasOwnProperty('mensagem') && result.erro.mensagem.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_error_code = result.erro.codigo[0];
          res.cielo_error_message = result.erro.mensagem[0];
          res.cielo_detailed_error_message = translateCieloErrorCode(res.cielo_error_code.slice(-2));
          return callback(err, res);
        }

        // if success response, check if response contains all the required fields
        if (result.hasOwnProperty('retorno-token')) {
          if (!(result['retorno-token'].hasOwnProperty('token') && result['retorno-token'].token.length > 0
                && result['retorno-token'].token[0].hasOwnProperty('dados-token') && result['retorno-token'].token[0]['dados-token'].length > 0
                && result['retorno-token'].token[0]['dados-token'][0].hasOwnProperty('codigo-token') && result['retorno-token'].token[0]['dados-token'][0]['codigo-token'].length > 0
                && result['retorno-token'].token[0]['dados-token'][0].hasOwnProperty('status') && result['retorno-token'].token[0]['dados-token'][0]['status'].length > 0
                && result['retorno-token'].token[0]['dados-token'][0].hasOwnProperty('numero-cartao-truncado') && result['retorno-token'].token[0]['dados-token'][0]['numero-cartao-truncado'].length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_card_token = result['retorno-token'].token[0]['dados-token'][0]['codigo-token'][0];
          res.cielo_card_token_status = result['retorno-token'].token[0]['dados-token'][0]['status'][0];
          res.cielo_card_token_detailed_status = (res.cielo_card_token_status === 0 ? 'Cartão bloqueado' : 'Cartão desbloqueado');
          res.cielo_card_masked_pan = result['retorno-token'].token[0]['dados-token'][0]['numero-cartao-truncado'][0];
          return callback(err, res);
        }

        callback('Erro na resposta da Cielo');
      });
    });
  });
  req.write(postData);
  req.end();
};

exports.createTransaction = function _createTransaction(transaction, callback) {

  // verifica os campos da transação
  var objectValidator = new ObjectValidator({
    'merchant': {
      'value': {
        'affiliationNumber': {'types': ['string']},
        'affiliationKey': {'types': ['string']}
      }
    },
    'amount': {'types': ['string']},
    'metaId': {'types': ['string']},
    'softDescriptor': {'types': ['string']},
    'createdAt': {'types': ['string']},
    'capture': {'isOptional': true, 'types': ['boolean']},
    'type': {'types': ['string']},
    'returnURL': {'isOptional': true, 'types': ['string']},
    'creditCard': {
      'value': {
        'token': {'isOptional': true, 'types': ['string']},
        'pan': {'isOptional': true, 'types': ['string']},
        'expirationDate': {'isOptional': true, 'types': ['string']},
        'brand': {'types': ['string']},
        'cardHolderName': {'isOptional': true, 'types': ['string']},
        'cvv': {'isOptional': true, 'types': ['string']},
      }
    },
  });
  var err = objectValidator.validate(transaction);
  if (err) {
    return callback('Dados de entrada inválidos');
  }

  // transaction must contain credit card data XOR card token
  if ((!transaction.creditCard.hasOwnProperty('token') && !transaction.creditCard.hasOwnProperty('pan'))
      || (transaction.creditCard.hasOwnProperty('token') && transaction.creditCard.hasOwnProperty('pan'))) {
    return callback('Dados de entrada inválidos. Transação deve conter o pan ou o token do cartão.');
  }

  // if transaction contains the card pan, it must contain the expiration date and vice-versa
  if ((transaction.creditCard.hasOwnProperty('pan') && !transaction.creditCard.hasOwnProperty('expirationDate'))
      || (transaction.creditCard.hasOwnProperty('expirationDate') && !transaction.creditCard.hasOwnProperty('pan'))) {
    return callback('Dados de entrada inválidos. Transação deve conter pan e data de expiração.');
  }

  // transaction type must be credit or debit
  if (transaction.type !== 'credit' && transaction.type !== 'debit') {
    return callback('Dados de entrada inválidos. Tipo da transação deve ser "credit" ou "debit".');
  }

  // transaction must contain capture field if credit transaction
  if (transaction.type === 'credit' && !transaction.hasOwnProperty('capture')) {
    return callback('Dados de entrada inválidos. Transação deve conter o campo "capture" para transações de crédito.');
  }

  // transaction must not contain capture field if debit transaction
  if (transaction.type === 'debit' && transaction.hasOwnProperty('capture')) {
    return callback('Dados de entrada inválidos. Transação não deve conter o campo "capture" para transações de débito.');
  }

  // transaction must contain return URL if debit transaction
  if (transaction.type === 'debit' && !transaction.hasOwnProperty('returnURL')) {
    return callback('Dados de entrada inválidos. Transação deve conter a URL de retorno (returnURL) para transações de débito.');
  }

  // cria requisição para criar transação
  var req = {};

  // dados da transação inseridos na própria tag (obrigatório)
  req['@'] = {};
  req['@'].id = 'a97ab62a-7956-41ea-b03f-c2e9f612c293';
  req['@'].versao = config.API.version;

  // dados-ec (obrigatório)
  req['dados-ec'] = {};
  req['dados-ec'].numero = transaction.merchant.affiliationNumber; // número de afiliação da loja com a Cielo (obrigatório)
  req['dados-ec'].chave = transaction.merchant.affiliationKey; // chave de acesso da loja atribuída pela Cielo (obrigatório)

  // dados-portador (obrigatório)
  req['dados-portador'] = {};
  if (transaction.creditCard.hasOwnProperty('token')) {

    // token que deve ser utilizado em substituição aos dados do cartão para uma autorização direta ou uma transação recorrente
    // não é permitido o envio do token junto com os dados do cartão na mesma transação (condicional)
    req['dados-portador'].token = transaction.creditCard.token;
  }
  else {

    req['dados-portador'].numero = transaction.creditCard.pan; // número do cartão (obrigatório)
    req['dados-portador'].validade = transaction.creditCard.expirationDate; // validade do cartão no formato aaaamm (obrigatório)

    // indicador sobre o envio do código de segurança: 0 – não informado, 1 – informado, 2 – ilegível, 9 – inexistente
    req['dados-portador'].indicador = '0';
    if (transaction.creditCard.hasOwnProperty('cvv')) {
      req['dados-portador'].indicador = '1';
      req['dados-portador']['codigo-seguranca'] = transaction.creditCard.cvv; // código de segurança do cartão (obrigatório se indicador = 1)
    }
    if (transaction.creditCard.hasOwnProperty('cardHolderName')) {
      req['dados-portador']['nome-portador'] = transaction.creditCard.cardHolderName; // nome como impresso no cartão (opcional)
    }
  }

  // dados-pedido (obrigatório)
  req['dados-pedido'] = {};
  req['dados-pedido'].numero = transaction.metaId; // número do pedido da loja, recomenda-se que seja um valor único por pedido (obrigatório)
  req['dados-pedido'].valor = transaction.amount; // valor a ser cobrado pelo pedido (obrigatório)

  // indicador sobre o envio do código de segurança: 0 – não informado, 1 – informado, 2 – ilegível, 9 – inexistente
  req['dados-pedido'].moeda = '986'; // código numérico da moeda na norma ISO 4217. Para o Real, o código é 986 (obrigatório)
  req['dados-pedido']['data-hora'] = transaction.createdAt; // data hora do pedido. Formato: aaaa-MM-ddTHH24:mm:ss (obrigatório)
  req['dados-pedido']['data-hora'] = '2011-12-07T11:43:37';

  // req['dados-pedido'].descricao = ''; // descrição do pedido (opcional)
  // idioma do pedido: PT (português), EN (inglês) ou ES (espanhol)
  // com base nessa informação é definida a língua a ser utilizada nas telas da Cielo (opcional)
  req['dados-pedido'].idioma = 'PT';

  // texto de até 13 caracteres que será exibido na fatura do portador, após o nome do EC (opcional)
  req['dados-pedido']['soft-descriptor'] = transaction.softDescriptor;

  // forma-pagamento (obrigatório)
  req['forma-pagamento'] = {};

  // mome da bandeira (minúsculo): “visa”, “mastercard”, “diners”, “discover”, “elo”, “amex”, “jcb”, “aura” (obrigatório)
  req['forma-pagamento'].bandeira = transaction.creditCard.brand;

  // código do produto: 1 – Crédito à Vista, 2 – Parcelado loja, A – Débito (obrigatório)
  req['forma-pagamento'].produto = 'A'; // valor a ser cobrado pelo pedido (obrigatório)

  // código do produto: 1 – Crédito à Vista, 2 – Parcelado loja, A – Débito (obrigatório)
  if (transaction.type === 'credit') {
    req['forma-pagamento'].produto = '1'; // valor a ser cobrado pelo pedido (obrigatório)
  }
  else {
    req['forma-pagamento'].produto = 'A'; // valor a ser cobrado pelo pedido (obrigatório)
  }

  // número de parcelas. Para crédito à vista ou débito, utilizar 1 (obrigatório)
  req['forma-pagamento'].parcelas = '1';

  // url-retorno (obrigatório)
  // URL da página de retorno. É para essa página que a Cielo vai direcionar o browser ao fim da
  // autenticação ou da autorização. Não é obrigatório apenas para autorização direta,
  // porém o campo dever ser inserido como null
  if (transaction.type === 'credit') {
    req['url-retorno'] = 'null';
  }
  else {
    req['url-retorno'] = transaction.returnURL;
  }

  // autorizar (obrigatório)
  // indicador de autorização: 0 – Não autorizar (somente autenticar)
  // 1 – Autorizar somente se autenticada.
  // 2 – Autorizar autenticada e não autenticada.
  // 3 – Autorizar sem passar por autenticação (somente para crédito) – também conhecida como “Autorização Direta”.
  // Obs.: Para Diners, Discover, Elo, Amex, Aura e JCB o valor será sempre “3”,
  // pois estas bandeiras não possuem programa de autenticação.
  // 4 – Transação Recorrente
  if (transaction.type === 'credit') {
    req.autorizar = '3';
  }
  else {
    req.autorizar = '1';
  }

  // capturar (obrigatório)
  // true ou false. Define se a transação será automaticamente capturada caso seja autorizada
  if (transaction.type === 'debit' || (transaction.type === 'credit' && transaction.capture)) {
    req.capturar = 'true';
  }
  else {
    req.capturar = 'false';
  }

  // campo-livre (opcional)
  // campo livre disponível para o estabelecimento
  // req['campo-livre'] = '';

  // bin (opcional)
  // seis primeiros números do cartão
  // req.bin = '455187';

  // gerar-token (opcional)
  // true ou false. Define se a transação atual deve gerar um token associado ao cartão
  // req['gerar-token'] = 'false';

  // avs (opcional)
  // string contendo um bloco XML, encapsulado pelo CDATA,
  // contendo as informações necessárias para realizar a consulta ao serviço
  // req.avs = '';

  // parse XML
  var xml = js2xmlparser('requisicao-transacao', req, xmlParserOptions);
  var xmlRequest = '<?xml version="1.0" encoding="ISO-8859-1"?>\n' + xml;
  var postData = 'mensagem=' + xmlRequest;

  // send request
  requestOptions.headers['Content-Length'] = Buffer.byteLength(postData);
  var req = https.request(requestOptions, function _request(res) {
    var data = '';
    res.on('data', function _on(chunk) {
      data += iconv.decode(chunk, 'iso-8859-1');
    });
    res.on('error', function _on(e) {
      return callback(e);
    });
    res.on('end', function _on() {

      // parse XML response
      xml2js.parseString(data, function _parseString(err, result) {
        if (err) {
          return callback('Erro na resposta da Cielo');
        }

        // if error response, check if response contains all the required fields
        var res = {};
        if (result.hasOwnProperty('erro')) {
          if (!(result.erro.hasOwnProperty('codigo') && result.erro.codigo.length > 0
                && result.erro.hasOwnProperty('mensagem') && result.erro.mensagem.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_error_code = result.erro.codigo[0];
          res.cielo_error_message = result.erro.mensagem[0];
          res.cielo_detailed_error_message = translateCieloErrorCode(res.cielo_error_code.slice(-2));
          return callback(err, res);
        }

        // if success response, check if response contains all the required fields
        if (result.hasOwnProperty('transacao')) {
          if (!(result.transacao.hasOwnProperty('tid') && result.transacao.tid.length > 0
                && result.transacao.hasOwnProperty('status') && result.transacao.status.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_tid = result.transacao.tid[0];
          res.cielo_status = result.transacao.status[0];
          res.cielo_detailed_status = translateCieloTransactionStatus(res.cielo_status);

          // get authentication fields
          if (result.transacao.hasOwnProperty('autenticacao') && result.transacao.autenticacao.length > 0
              && result.transacao.autenticacao[0].hasOwnProperty('mensagem') && result.transacao.autenticacao[0].mensagem.length > 0) {
            res.cielo_authentication_message = result.transacao.autenticacao[0].mensagem[0];
          }

          // get authorization fields
          if (result.transacao.hasOwnProperty('autorizacao') && result.transacao.autorizacao.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('mensagem') && result.transacao.autorizacao[0].mensagem.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('data-hora') && result.transacao.autorizacao[0]['data-hora'].length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('valor') && result.transacao.autorizacao[0].valor.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('lr') && result.transacao.autorizacao[0].lr.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('nsu') && result.transacao.autorizacao[0].nsu.length > 0) {
            res.cielo_authorization_message = result.transacao.autorizacao[0].mensagem[0];
            res.cielo_authorization_date = result.transacao.autorizacao[0]['data-hora'][0];
            res.cielo_authorization_amount = result.transacao.autorizacao[0].valor[0];
            res.cielo_authorization_code = result.transacao.autorizacao[0].lr[0];
            res.cielo_authorization_detailed_code = translateCieloAuthCode(res.cielo_authorization_code);
            res.cielo_authorization_nsu = result.transacao.autorizacao[0].nsu[0];
          }

          // get capture fields
          if (result.transacao.hasOwnProperty('captura') && result.transacao.captura.length > 0
              && result.transacao.captura[0].hasOwnProperty('mensagem') && result.transacao.captura[0].mensagem.length > 0
              && result.transacao.captura[0].hasOwnProperty('data-hora') && result.transacao.captura[0]['data-hora'].length > 0
              && result.transacao.captura[0].hasOwnProperty('valor') && result.transacao.captura[0].valor.length > 0) {
            res.cielo_capture_message = result.transacao.captura[0].mensagem[0];
            res.cielo_capture_date = result.transacao.captura[0]['data-hora'][0];
            res.cielo_capture_amount = result.transacao.captura[0].valor[0];
          }

          // get cancellation fields
          if (result.transacao.hasOwnProperty('cancelamentos') && result.transacao.cancelamentos.length > 0
              && result.transacao.cancelamentos[0].hasOwnProperty('cancelamento') && result.transacao.cancelamentos[0].cancelamento.length > 0) {
            var cancellations = [];
            for (var i = 0; i < result.transacao.cancelamentos[0].cancelamento.length; i++) {
              if (result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('mensagem') && result.transacao.cancelamentos[0].cancelamento[i].mensagem.length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('data-hora') && result.transacao.cancelamentos[0].cancelamento[i]['data-hora'].length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('valor') && result.transacao.cancelamentos[0].cancelamento[i].valor.length > 0) {
                var cancellation = {};
                cancellation.cancellation_message = result.transacao.cancelamentos[0].cancelamento[i].mensagem[0];
                cancellation.cancellation_date = result.transacao.cancelamentos[0].cancelamento[i]['data-hora'][0];
                cancellation.cancellation_amount = result.transacao.cancelamentos[0].cancelamento[i].valor[0];
                cancellations.push(cancellation);
              }
            }
            res.cielo_cancellations = cancellations;
          }

          // get authorization URL
          if (result.transacao.hasOwnProperty('url-autenticacao') && result.transacao['url-autenticacao'].length > 0) {
            res.cielo_auth_url = result.transacao['url-autenticacao'][0];
          }

          return callback(err, res);
        }

        callback('Erro na resposta da Cielo');
      });
    });
  });
  req.write(postData);
  req.end();
};

exports.captureTransaction = function _captureTransaction(transaction, callback) {

  // verifica os campos da transação
  var objectValidator = new ObjectValidator({
    'merchant': {
      'value': {
        'affiliationNumber': {'types': ['string']},
        'affiliationKey': {'types': ['string']}
      }
    },
    'tid': {'types': ['string']},
    'amount': {'isOptional': true, 'types': ['string']}
  });
  var err = objectValidator.validate(transaction);
  if (err) {
    return callback('Dados de entrada inválidos');
  }

  // cria requisição para consultar transação
  var req = {};

  // dados da transação inseridos na própria tag (obrigatório)
  req['@'] = {};
  req['@'].id = 'a97ab62a-7956-41ea-b03f-c2e9f612c293';
  req['@'].versao = config.API.version;

  // transaction id (obrigatório)
  req.tid = transaction.tid; // identificador da transação

  // dados-ec (obrigatório)
  req['dados-ec'] = {};
  req['dados-ec'].numero = transaction.merchant.affiliationNumber; // número de afiliação da loja com a Cielo (obrigatório)
  req['dados-ec'].chave = transaction.merchant.affiliationKey; // chave de acesso da loja atribuída pela Cielo (obrigatório)

  // valor (opcional)
  // valor a ser capturado. Caso não seja informado, será uma captura total
  if (transaction.hasOwnProperty('amount')) {
    req.valor = transaction.amount;
  }

  // parse XML
  var xml = js2xmlparser('requisicao-captura', req, xmlParserOptions);
  var xmlRequest = '<?xml version="1.0" encoding="ISO-8859-1"?>\n' + xml;
  var postData = 'mensagem=' + xmlRequest;

  // send request
  requestOptions.headers['Content-Length'] = Buffer.byteLength(postData);
  var req = https.request(requestOptions, function _request(res) {
    var data = '';
    res.on('data', function _on(chunk) {
      data += iconv.decode(chunk, 'iso-8859-1');
    });
    res.on('error', function _on(e) {
      return callback(e);
    });
    res.on('end', function _on() {

      // parse XML response
      xml2js.parseString(data, function _parseString(err, result) {
        if (err) {
          return callback('Erro na resposta da Cielo');
        }

        // if error response, check if response contains all the required fields
        var res = {};
        if (result.hasOwnProperty('erro')) {
          if (!(result.erro.hasOwnProperty('codigo') && result.erro.codigo.length > 0
                && result.erro.hasOwnProperty('mensagem') && result.erro.mensagem.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_error_code = result.erro.codigo[0];
          res.cielo_error_message = result.erro.mensagem[0];
          res.cielo_detailed_error_message = translateCieloErrorCode(res.cielo_error_code.slice(-2));
          return callback(err, res);
        }

        // if success response, check if response contains all the required fields
        if (result.hasOwnProperty('transacao')) {
          if (!(result.transacao.hasOwnProperty('tid') && result.transacao.tid.length > 0
                && result.transacao.hasOwnProperty('status') && result.transacao.status.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_tid = result.transacao.tid[0];
          res.cielo_status = result.transacao.status[0];
          res.cielo_detailed_status = translateCieloTransactionStatus(res.cielo_status);

          // get authentication fields
          if (result.transacao.hasOwnProperty('autenticacao') && result.transacao.autenticacao.length > 0
              && result.transacao.autenticacao[0].hasOwnProperty('mensagem') && result.transacao.autenticacao[0].mensagem.length > 0) {
            res.cielo_authentication_message = result.transacao.autenticacao[0].mensagem[0];
          }

          // get authorization fields
          if (result.transacao.hasOwnProperty('autorizacao') && result.transacao.autorizacao.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('mensagem') && result.transacao.autorizacao[0].mensagem.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('data-hora') && result.transacao.autorizacao[0]['data-hora'].length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('valor') && result.transacao.autorizacao[0].valor.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('lr') && result.transacao.autorizacao[0].lr.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('nsu') && result.transacao.autorizacao[0].nsu.length > 0) {
            res.cielo_authorization_message = result.transacao.autorizacao[0].mensagem[0];
            res.cielo_authorization_date = result.transacao.autorizacao[0]['data-hora'][0];
            res.cielo_authorization_amount = result.transacao.autorizacao[0].valor[0];
            res.cielo_authorization_code = result.transacao.autorizacao[0].lr[0];
            res.cielo_authorization_detailed_code = translateCieloAuthCode(res.cielo_authorization_code);
            res.cielo_authorization_nsu = result.transacao.autorizacao[0].nsu[0];
          }

          // get capture fields
          if (result.transacao.hasOwnProperty('captura') && result.transacao.captura.length > 0
              && result.transacao.captura[0].hasOwnProperty('mensagem') && result.transacao.captura[0].mensagem.length > 0
              && result.transacao.captura[0].hasOwnProperty('data-hora') && result.transacao.captura[0]['data-hora'].length > 0
              && result.transacao.captura[0].hasOwnProperty('valor') && result.transacao.captura[0].valor.length > 0) {
            res.cielo_capture_message = result.transacao.captura[0].mensagem[0];
            res.cielo_capture_date = result.transacao.captura[0]['data-hora'][0];
            res.cielo_capture_amount = result.transacao.captura[0].valor[0];
          }

          // get cancellation fields
          if (result.transacao.hasOwnProperty('cancelamentos') && result.transacao.cancelamentos.length > 0
              && result.transacao.cancelamentos[0].hasOwnProperty('cancelamento') && result.transacao.cancelamentos[0].cancelamento.length > 0) {
            var cancellations = [];
            for (var i = 0; i < result.transacao.cancelamentos[0].cancelamento.length; i++) {
              if (result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('mensagem') && result.transacao.cancelamentos[0].cancelamento[i].mensagem.length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('data-hora') && result.transacao.cancelamentos[0].cancelamento[i]['data-hora'].length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('valor') && result.transacao.cancelamentos[0].cancelamento[i].valor.length > 0) {
                var cancellation = {};
                cancellation.cancellation_message = result.transacao.cancelamentos[0].cancelamento[i].mensagem[0];
                cancellation.cancellation_date = result.transacao.cancelamentos[0].cancelamento[i]['data-hora'][0];
                cancellation.cancellation_amount = result.transacao.cancelamentos[0].cancelamento[i].valor[0];
                cancellations.push(cancellation);
              }
            }
            res.cielo_cancellations = cancellations;
          }

          return callback(err, res);
        }

        callback('Erro na resposta da Cielo');
      });
    });
  });
  req.write(postData);
  req.end();
};

exports.checkTransaction = function _checkTransaction(transaction, callback) {

  // verifica os campos da transação
  var objectValidator = new ObjectValidator({
    'merchant': {
      'value': {
        'affiliationNumber': {'types': ['string']},
        'affiliationKey': {'types': ['string']}
      }
    },
    'tid': {'isOptional': true, 'types': ['string']},
    'metaId': {'isOptional': true, 'types': ['string']}
  });
  var err = objectValidator.validate(transaction);
  if (err) {
    return callback('Dados de entrada inválidos');
  }

  // cria requisição para consultar transação
  var req = {};

  // dados da transação inseridos na própria tag (obrigatório)
  req['@'] = {};
  req['@'].id = 'a97ab62a-7956-41ea-b03f-c2e9f612c293';
  req['@'].versao = config.API.version;

  // a consulta pelo tid e pelo número da requisição são diferentes,
  // dá preferência pela consulta utilizando o tid
  if (transaction.hasOwnProperty('tid')) {
    req.tid = transaction.tid; // identificador da transação
  }
  else if (transaction.hasOwnProperty('metaId')) {
    req['numero-pedido'] = transaction.metaId; // número do pedido
  }
  else {
    return callback('Dados de entrada inválidos (transação sem tid e sem número de requisição)');
  }

  // dados-ec (obrigatório)
  req['dados-ec'] = {};
  req['dados-ec'].numero = transaction.merchant.affiliationNumber; // número de afiliação da loja com a Cielo (obrigatório)
  req['dados-ec'].chave = transaction.merchant.affiliationKey; // chave de acesso da loja atribuída pela Cielo (obrigatório)

  // parse XML
  var xml;
  if (transaction.hasOwnProperty('tid')) {
    xml = js2xmlparser('requisicao-consulta', req, xmlParserOptions);
  }
  else if (transaction.hasOwnProperty('metaId')) {
    xml = js2xmlparser('requisicao-consulta-chsec', req, xmlParserOptions);
  }
  else {
    return callback('Dados de entrada inválidos (transação sem tid e sem número de requisição)');
  }
  var xmlRequest = '<?xml version="1.0" encoding="ISO-8859-1"?>\n' + xml;
  var postData = 'mensagem=' + xmlRequest;

  // send request
  requestOptions.headers['Content-Length'] = Buffer.byteLength(postData);
  var req = https.request(requestOptions, function _request(res) {
    var data = '';
    res.on('data', function _on(chunk) {
      data += iconv.decode(chunk, 'iso-8859-1');
    });
    res.on('error', function _on(e) {
      return callback(e);
    });
    res.on('end', function _on() {

      // parse XML response
      xml2js.parseString(data, function _parseString(err, result) {
        if (err) {
          return callback('Erro na resposta da Cielo');
        }

        // if error response, check if response contains all the required fields
        var res = {};
        if (result.hasOwnProperty('erro')) {
          if (!(result.erro.hasOwnProperty('codigo') && result.erro.codigo.length > 0
                && result.erro.hasOwnProperty('mensagem') && result.erro.mensagem.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_error_code = result.erro.codigo[0];
          res.cielo_error_message = result.erro.mensagem[0];
          res.cielo_detailed_error_message = translateCieloErrorCode(res.cielo_error_code.slice(-2));
          return callback(err, res);
        }

        // if success response, check if response contains all the required fields
        if (result.hasOwnProperty('transacao')) {
          if (!(result.transacao.hasOwnProperty('tid') && result.transacao.tid.length > 0
                && result.transacao.hasOwnProperty('status') && result.transacao.status.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_tid = result.transacao.tid[0];
          res.cielo_status = result.transacao.status[0];
          res.cielo_detailed_status = translateCieloTransactionStatus(res.cielo_status);

          // get authentication fields
          if (result.transacao.hasOwnProperty('autenticacao') && result.transacao.autenticacao.length > 0
              && result.transacao.autenticacao[0].hasOwnProperty('mensagem') && result.transacao.autenticacao[0].mensagem.length > 0) {
            res.cielo_authentication_message = result.transacao.autenticacao[0].mensagem[0];
          }

          // get authorization fields
          if (result.transacao.hasOwnProperty('autorizacao') && result.transacao.autorizacao.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('mensagem') && result.transacao.autorizacao[0].mensagem.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('data-hora') && result.transacao.autorizacao[0]['data-hora'].length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('valor') && result.transacao.autorizacao[0].valor.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('lr') && result.transacao.autorizacao[0].lr.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('nsu') && result.transacao.autorizacao[0].nsu.length > 0) {
            res.cielo_authorization_message = result.transacao.autorizacao[0].mensagem[0];
            res.cielo_authorization_date = result.transacao.autorizacao[0]['data-hora'][0];
            res.cielo_authorization_amount = result.transacao.autorizacao[0].valor[0];
            res.cielo_authorization_code = result.transacao.autorizacao[0].lr[0];
            res.cielo_authorization_detailed_code = translateCieloAuthCode(res.cielo_authorization_code);
            res.cielo_authorization_nsu = result.transacao.autorizacao[0].nsu[0];
          }

          // get capture fields
          if (result.transacao.hasOwnProperty('captura') && result.transacao.captura.length > 0
              && result.transacao.captura[0].hasOwnProperty('mensagem') && result.transacao.captura[0].mensagem.length > 0
              && result.transacao.captura[0].hasOwnProperty('data-hora') && result.transacao.captura[0]['data-hora'].length > 0
              && result.transacao.captura[0].hasOwnProperty('valor') && result.transacao.captura[0].valor.length > 0) {
            res.cielo_capture_message = result.transacao.captura[0].mensagem[0];
            res.cielo_capture_date = result.transacao.captura[0]['data-hora'][0];
            res.cielo_capture_amount = result.transacao.captura[0].valor[0];
          }

          // get cancellation fields
          if (result.transacao.hasOwnProperty('cancelamentos') && result.transacao.cancelamentos.length > 0
              && result.transacao.cancelamentos[0].hasOwnProperty('cancelamento') && result.transacao.cancelamentos[0].cancelamento.length > 0) {
            var cancellations = [];
            for (var i = 0; i < result.transacao.cancelamentos[0].cancelamento.length; i++) {
              if (result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('mensagem') && result.transacao.cancelamentos[0].cancelamento[i].mensagem.length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('data-hora') && result.transacao.cancelamentos[0].cancelamento[i]['data-hora'].length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('valor') && result.transacao.cancelamentos[0].cancelamento[i].valor.length > 0) {
                var cancellation = {};
                cancellation.cancellation_message = result.transacao.cancelamentos[0].cancelamento[i].mensagem[0];
                cancellation.cancellation_date = result.transacao.cancelamentos[0].cancelamento[i]['data-hora'][0];
                cancellation.cancellation_amount = result.transacao.cancelamentos[0].cancelamento[i].valor[0];
                cancellations.push(cancellation);
              }
            }
            res.cielo_cancellations = cancellations;
          }

          return callback(err, res);
        }

        callback('Erro na resposta da Cielo');
      });
    });
  });
  req.write(postData);
  req.end();
};

exports.cancelTransaction = function _cancelTransaction(transaction, callback) {

  // verifica os campos da transação
  var objectValidator = new ObjectValidator({
    'merchant': {
      'value': {
        'affiliationNumber': {'types': ['string']},
        'affiliationKey': {'types': ['string']}
      }
    },
    'tid': {'types': ['string']},
    'amount': {'isOptional': true, 'types': ['string']},
  });
  var err = objectValidator.validate(transaction);
  if (err) {
    return callback('Dados de entrada inválidos');
  }

  // cria requisição para cancelar transação
  var req = {};

  // dados da transação inseridos na própria tag (obrigatório)
  req['@'] = {};
  req['@'].id = 'a97ab62a-7956-41ea-b03f-c2e9f612c293';
  req['@'].versao = config.API.version;

  // transaction id (obrigatório)
  req.tid = transaction.tid; // identificador da transação

  // dados-ec (obrigatório)
  req['dados-ec'] = {};
  req['dados-ec'].numero = transaction.merchant.affiliationNumber; // número de afiliação da loja com a Cielo (obrigatório)
  req['dados-ec'].chave = transaction.merchant.affiliationKey; // chave de acesso da loja atribuída pela Cielo (obrigatório)

  // valor (opcional)
  // valor a ser cancelado. Caso não seja informado, será um cancelamento total
  if (transaction.hasOwnProperty('amount')) {
    req.valor = transaction.amount;
  }

  // parse XML
  var xml = js2xmlparser('requisicao-cancelamento', req, xmlParserOptions);
  var xmlRequest = '<?xml version="1.0" encoding="ISO-8859-1"?>\n' + xml;
  var postData = 'mensagem=' + xmlRequest;

  // send request
  requestOptions.headers['Content-Length'] = Buffer.byteLength(postData);
  var req = https.request(requestOptions, function _request(res) {
    var data = '';
    res.on('data', function _on(chunk) {
      data += iconv.decode(chunk, 'iso-8859-1');
    });
    res.on('error', function _on(e) {
      return callback(e);
    });
    res.on('end', function _on() {

      // parse XML response
      xml2js.parseString(data, function _parseString(err, result) {
        if (err) {
          return callback(err);
        }

        // if error response, check if response contains all the required fields
        var res = {};
        if (result.hasOwnProperty('erro')) {
          if (!(result.erro.hasOwnProperty('codigo') && result.erro.codigo.length > 0
                && result.erro.hasOwnProperty('mensagem') && result.erro.mensagem.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_error_code = result.erro.codigo[0];
          res.cielo_error_message = result.erro.mensagem[0];
          res.cielo_detailed_error_message = translateCieloErrorCode(res.cielo_error_code.slice(-2));
          return callback(err, res);
        }

        // if success response, check if response contains all the required fields
        if (result.hasOwnProperty('transacao')) {
          if (!(result.transacao.hasOwnProperty('tid') && result.transacao.tid.length > 0
                && result.transacao.hasOwnProperty('status') && result.transacao.status.length > 0)) {
            return callback('Erro na resposta da Cielo');
          }

          // create response fields
          res.cielo_tid = result.transacao.tid[0];
          res.cielo_status = result.transacao.status[0];
          res.cielo_detailed_status = translateCieloTransactionStatus(res.cielo_status);

          // get authentication fields
          if (result.transacao.hasOwnProperty('autenticacao') && result.transacao.autenticacao.length > 0
              && result.transacao.autenticacao[0].hasOwnProperty('mensagem') && result.transacao.autenticacao[0].mensagem.length > 0) {
            res.cielo_authentication_message = result.transacao.autenticacao[0].mensagem[0];
          }

          // get authorization fields
          if (result.transacao.hasOwnProperty('autorizacao') && result.transacao.autorizacao.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('mensagem') && result.transacao.autorizacao[0].mensagem.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('data-hora') && result.transacao.autorizacao[0]['data-hora'].length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('valor') && result.transacao.autorizacao[0].valor.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('lr') && result.transacao.autorizacao[0].lr.length > 0
              && result.transacao.autorizacao[0].hasOwnProperty('nsu') && result.transacao.autorizacao[0].nsu.length > 0) {
            res.cielo_authorization_message = result.transacao.autorizacao[0].mensagem[0];
            res.cielo_authorization_date = result.transacao.autorizacao[0]['data-hora'][0];
            res.cielo_authorization_amount = result.transacao.autorizacao[0].valor[0];
            res.cielo_authorization_code = result.transacao.autorizacao[0].lr[0];
            res.cielo_authorization_detailed_code = translateCieloAuthCode(res.cielo_authorization_code);
            res.cielo_authorization_nsu = result.transacao.autorizacao[0].nsu[0];
          }

          // get capture fields
          if (result.transacao.hasOwnProperty('captura') && result.transacao.captura.length > 0
              && result.transacao.captura[0].hasOwnProperty('mensagem') && result.transacao.captura[0].mensagem.length > 0
              && result.transacao.captura[0].hasOwnProperty('data-hora') && result.transacao.captura[0]['data-hora'].length > 0
              && result.transacao.captura[0].hasOwnProperty('valor') && result.transacao.captura[0].valor.length > 0) {
            res.cielo_capture_message = result.transacao.captura[0].mensagem[0];
            res.cielo_capture_date = result.transacao.captura[0]['data-hora'][0];
            res.cielo_capture_amount = result.transacao.captura[0].valor[0];
          }

          // get cancellation fields
          if (result.transacao.hasOwnProperty('cancelamentos') && result.transacao.cancelamentos.length > 0
              && result.transacao.cancelamentos[0].hasOwnProperty('cancelamento') && result.transacao.cancelamentos[0].cancelamento.length > 0) {
            var cancellations = [];
            for (var i = 0; i < result.transacao.cancelamentos[0].cancelamento.length; i++) {
              if (result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('mensagem') && result.transacao.cancelamentos[0].cancelamento[i].mensagem.length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('data-hora') && result.transacao.cancelamentos[0].cancelamento[i]['data-hora'].length > 0
                  && result.transacao.cancelamentos[0].cancelamento[i].hasOwnProperty('valor') && result.transacao.cancelamentos[0].cancelamento[i].valor.length > 0) {
                var cancellation = {};
                cancellation.cancellation_message = result.transacao.cancelamentos[0].cancelamento[i].mensagem[0];
                cancellation.cancellation_date = result.transacao.cancelamentos[0].cancelamento[i]['data-hora'][0];
                cancellation.cancellation_amount = result.transacao.cancelamentos[0].cancelamento[i].valor[0];
                cancellations.push(cancellation);
              }
            }
            res.cielo_cancellations = cancellations;
          }

          return callback(err, res);
        }

        callback('Erro na resposta da Cielo');
      });
    });
  });
  req.write(postData);
  req.on('error', function _on(e) {
    callback(e);
  });
  req.end();
};
