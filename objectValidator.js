'use strict';

function ObjectValidator(schema) {
  this.schema = schema;
}

// custom function for returning errors
function createError(type, field, message) {
  var error = {
    type   : type,
    field  : field,
    message: message
  };
  return error;
}

ObjectValidator.prototype.validate = function _validate(request) {

  function traverse(schema, request, prefix) {

    // for each key in the request
    for (var key in schema) {

      // if key is optional in the schema, and request does not contain it, ignore it
      if (schema[key].hasOwnProperty('isOptional') && schema[key]['isOptional'] && !(request.hasOwnProperty(key))) {
        continue;
      }

      // if key does not exist in the request, create error
      if (!request.hasOwnProperty(key)) {
        return createError('required', prefix + key, 'Parâmetro "' + prefix + key + '" não encontrado');
      }

      // if schema allows null and request contains null. ignore it
      if (schema[key].hasOwnProperty('allowNull') && schema[key]['allowNull'] && request[key] === null) {
        continue;
      }

      // if request contains null, create error
      if (request[key] === null) {
        return createError('type', prefix + key, 'Parâmetro "' + prefix + key + '" é do tipo incorreto');
      }

      // if schema contains valid types for this key
      if (schema[key].hasOwnProperty('types')) {

        // check if request value belongs to one of these types
        var validType = false;
        for (var i in schema[key]['types']) {
          if (typeof request[key] === schema[key]['types'][i]) {
            validType = true;
            break;
          }
        }

        // if request value is not o f a valid type, create error
        if (!validType) {
          return createError('type', prefix + key, 'Parâmetro "' + prefix + key + '" é do tipo incorreto');
        }
      }

      // if schema contains a valid value for this key
      else if (schema[key].hasOwnProperty('value')) {

        // check if request also contains an object for this key
        if (typeof request[key] !== 'object') {
          return createError('type', prefix + key, 'Parâmetro "' + prefix + key + '" é do tipo incorreto');
        }

        var error = traverse(schema[key]['value'], request[key], prefix + key + '.');
        if (error) {
          return error;
        }
      }

      // should not reach here, schema is invalid
      else {
        return createError('schema', prefix + key, 'Schema é inválido');
      }
    }

    return null;
  }

  return traverse(this.schema, request, '');
};

module.exports = ObjectValidator;
