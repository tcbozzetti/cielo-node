'use strict';

var path = require('path');
var moment = require('moment');

var cielo = require(path.join(__dirname, 'cielo'));

/*
cielo.createCardToken({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  creditCard: {
    pan: '3566007770004971',
    expirationDate: '201805',
    cardHolderName: 'Joao'
  }
}, function _createCardToken(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.createTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  amount: '5500',
  metaId: '954756320',
  softDescriptor: 'produto X',
  createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:SS'),
  capture: true,
  type: 'credit',
  creditCard: {
    pan: '3566007770004971',
    expirationDate: '201805',
    cvv: '123',
    brand: 'jcb'
  }
}, function _createTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.createTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  amount: '5500',
  metaId: '954756320',
  softDescriptor: 'produto X',
  createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:SS'),
  capture: false,
  type: 'credit',
  creditCard: {
    pan: '3566007770004971',
    expirationDate: '201805',
    cvv: '123',
    brand: 'jcb'
  }
}, function _createTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.createTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  amount: '5500',
  metaId: '954756320',
  softDescriptor: 'produto X',
  createdAt: moment.utc().format('YYYY-MM-DDTHH:mm:SS'),
  type: 'debit',
  returnURL: 'http://devapi.4all.mobi:3050/pedido',
  creditCard: {
    pan: '4012001037141112',
    expirationDate: '201805',
    cvv: '123',
    brand: 'visa'
  }
}, function _createTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.captureTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  tid: '1006993069000668A8FA',
  amount: '1000'
}, function _captureTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.checkTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  tid: '1006993069000668A8FA'
}, function _checkTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.checkTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  metaId: '954756320'
}, function _checkTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

/*
cielo.cancelTransaction({
  merchant: {
    affiliationNumber: '1006993069',
    affiliationKey: '25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3'
  },
  tid: '1006993069000668A8FA'
}, function _cancelTransaction(err, data) {
  console.log(err);
  console.log(data);
});
*/

